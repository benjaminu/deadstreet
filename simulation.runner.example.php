<?php

// Data source: http://publicdata.landregistry.gov.uk/market-trend-data/price-paid-data/a/pp-2014.csv

$eo = new ExtinctionObserver();
$eo->setZombieHordePopulation(10000); // Optional; defaults to 10000
$eo->setZombieDispersalFactor(10); // Optional; defaults to 10
$eo->setPostcodeAreaToAttack('HU'); // Optional
$eo->setDaysToSimulate(1000); // Optional
$eo->setReportIntervalInDays(20); // Optional; default report at end of days

$eo->run();

/**
 * Day number       Houses surviving        Houses destroyed        Zombie population       Households infected     Value of damage
 * 1                990                     10                      10001                   1                       £1000000
 * 2                500                     500                     10400                   399                     £29239439
 * ...
 * End Of Days      0                       1000                    10700                   300                     £1000000000
 */
