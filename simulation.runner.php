<?php

ini_set('memory_limit', '4096M');
ini_set('max_execution_time', '36000');
require_once __DIR__ . '/vendor/autoload.php';

use OutbreakSimulation\OutbreakSimulator;

if (! defined('STDIN') ) {
    echo "Please run via CLI\n";
    exit(0);
}

$os = new OutbreakSimulator;
$os->setDataSource(__DIR__ . '/price-data/pp-sample.csv');
$os->setDurationOfAttackToSimulate(1000);
$os->setZombieHordePopulation(10000);
$os->setReportIntervalInDays(20);
$os->setDefenceAllowed(true);

$os->run();
exit(1);