<?php
namespace OutbreakSimulation;

/**
 * Detached House class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
class Detached extends House
{
    /**
     * {@inheritdoc}
     */
    protected $zombieFractionMultiplier = 0.2;
}