<?php
namespace OutbreakSimulation;

/**
 * Terraced House class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
class Terraced extends House
{
    /**
     * {@inheritdoc}
     */
    protected $zombieFractionMultiplier = 0.1;
}