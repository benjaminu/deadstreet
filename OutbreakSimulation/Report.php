<?php
namespace OutbreakSimulation;

/**
 * Report class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
class Report
{
    /**
     * Prints text to screen.
     *
     * @param string $message Message to print to screen.
     *
     * @return void
     */
    protected function displayMessage($message)
    {
        print("{$message}\n");
    }

    /**
     * Prints welcome message to screen.
     *
     * @param integer $title Application title.
     *
     * @return void
     */
    public function welcomeMessage($title)
    {
        $message = <<<EOD
================================================================================================================================
****************************************************** $title *****************************************************
EOD;
        $this->displayMessage($message);
    }

    /**
     * Prints loading message to screen.
     *
     * @return void
     */
    public function loadingMessage()
    {
        $message = <<<EOD
================================================================================================================================
******************************************** Loading Data - this may take a while... *******************************************
EOD;
        $this->displayMessage($message);
    }

    /**
     * Disaplays total number of houses.
     *
     * @param integer $total Total number of houses.
     *
     * @return void
     */
    public function totalHousesMessage($total)
    {
        $message = <<<EOD
================================================================================================================================
************************************************ Total number of houses - $total ***********************************************
EOD;
        $this->displayMessage($message);
    }

    /**
     * Disaplays total number of houses.
     *
     * @return void
     */
    public function displayHeaders()
    {
        $message = <<<EOD
================================================================================================================================
Day number\t Houses surviving\t Houses destroyed\t Zombie population\t Households infected\t Value of damage
================================================================================================================================
EOD;
        $this->displayMessage($message);
    }

    /**
     * Prints report data.
     *
     * @param  integer $day              Days passed so far.
     * @param  integer $remainingHouses  Number of surviving houses.
     * @param  integer $housesDestroyed  Number of houses destroyed so far.
     * @param  integer $zombiePopulation Current zombie population.
     * @param  integer $infections       Number of new zombies since last report.
     * @param  flaot   $damageDealt      Total damage dealt by Zombies so far.
     *
     * @return void
     */
    public function printReport(
        $day,
        $remainingHouses,
        $housesDestroyed,
        $zombiePopulation,
        $infections,
        $damageDealt
    )
    {
        if (! $remainingHouses) {
            $day = 'End of days';
        } else {
            $day .= "\t ";
        }

        $message = <<<EOD
{$day}\t {$remainingHouses}\t \t \t {$housesDestroyed}\t \t \t {$zombiePopulation}\t \t \t {$infections}\t \t \t {$damageDealt}
================================================================================================================================
EOD;
        $this->displayMessage($message);
    }

    /**
     * Prints goodbye message to screen.
     *
     * @return string
     */
    public function goodbyeMessage()
    {
        $message = <<<EOD
****************************************************** END OF SIMULATION!! *****************************************************
================================================================================================================================
******************************** Developer: Benjamin Ugbene <benjamin.ugbene@googlemail.com> ***********************************
================================================================================================================================
EOD;
        $this->displayMessage($message);
    }
}