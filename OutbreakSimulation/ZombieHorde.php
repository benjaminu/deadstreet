<?php
namespace OutbreakSimulation;

/**
 * ZombieHorde class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
class ZombieHorde
{
    /**
     * Damage exacted by an attack.
     *
     * @var integer
     */
    const ATTACK_VALUE = '10000';

    /**
     * Initial population of zombie horde.
     *
     * @var integer
     */
    protected $zombieHordePopulation = 10000;

    /**
     * List of zombies yet to attack
     *
     * @var integer
     */
    protected $zombiesYetToAttack;

    /**
     * Total number of households infected by horde.
     *
     * @var integer
     */
    protected $totalHouseholdsInfected = 0;

    /**
     * Total value of damage dealt by horde.
     *
     * @var integer
     */
    protected $totalValueOfDamageDealt = 0;

    /**
     * Sets the initial population of zombie horde.
     *
     * @param integer $zombiePopulation Initial population of zombie horde
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function setZombieHordePopulation($zombiePopulation)
    {
        if ($zombiePopulation) {
            $this->zombieHordePopulation = $zombiePopulation;
        }

        return $this;
    }

    /**
     * Returns the initial population of zombie horde.
     *
     * @return integer
     */
    public function getZombieHordePopulation()
    {
        return $this->zombieHordePopulation;
    }

    /**
     * Sets number zombies in zombie horde that are yet to attack.
     *
     * @param integer $value
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function setZombiesYetToAttack($value)
    {
        $this->zombiesYetToAttack = $value;

        return $this;
    }

    /**
     * Returns number zombies in zombie horde that are yet to attack.
     *
     * @return integer
     */
    public function getZombiesYetToAttack()
    {
        return $this->zombiesYetToAttack;
    }

    /**
     * Decreases population of zombies.
     *
     * @param integer $value Value by which to reduce size of zombie population.
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function decreaseZombieHorde($value)
    {
        $this->zombieHordePopulation -= $value;
        if ($this->zombieHordePopulation < 0) {
            $this->zombieHordePopulation = 0;
        }

        $this->zombiesYetToAttack -= $value;
        if ($this->zombiesYetToAttack < 0) {
            $this->zombiesYetToAttack = 0;
        }

        return $this;
    }

    /**
     * Increases the total number of households infected by horde.
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function incrementTotalHouseholdsInfected()
    {
        $this->totalHouseholdsInfected++;

        return $this;
    }

    /**
     * Returns total number of households infected by horde.
     *
     * @return integer
     */
    public function getTotalHouseholdsInfected()
    {
        return $this->totalHouseholdsInfected;
    }

    /**
     * Resets total number of households infected by horde.
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function resetTotalHouseholdsInfected()
    {
        $this->totalHouseholdsInfected = 0;

        return $this;
    }

    /**
     * Returns value of damage dealt by horde.
     *
     * @return integer
     */
    public function getTotalValueOfDamageDealt()
    {
        return $this->totalValueOfDamageDealt;
    }

    /**
     * Simulate an attack on a house.
     *
     * @param \OutbreakSimulation\House $house
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function attack(House &$house)
    {
        // Select fraction of Zombie Horde to attack
        $fractionMultiplier     = $house->getZombieFractionMultiplier();
        $maxNumberInAttackHorde = $fractionMultiplier * $this->zombiesYetToAttack;
        $hordeFractionToAttack  = rand(1, $maxNumberInAttackHorde);
        $attackCount            = 0;

        for($i = 0; $i < $hordeFractionToAttack; $i++) {
            // Round to nearest attack value
            if ($this->round($house->getPrice()) >= static::ATTACK_VALUE) {
                $housePrice = $house->getPrice();
                $house->decreasePrice(static::ATTACK_VALUE);
                $this->totalValueOfDamageDealt += static::ATTACK_VALUE;
                $attackCount++;
            }

            // Check if house has been completely destroyed.
            if ($house->getPrice() < static::ATTACK_VALUE) {
                // Add remaining house value to damage dealt
                if ($house->getPrice() < 0) {
                    $this->totalValueOfDamageDealt += $housePrice;
                } else {
                    $this->totalValueOfDamageDealt += $house->getPrice();
                }

                $house->setPrice(0);

                if ($this->isZombieSpawned()) {
                    // Increase size of Zombie Population
                    $this->zombieHordePopulation++;

                    // New Zombie should have a chance to attack
                    $this->zombiesYetToAttack++;

                    // Increase number of households infected
                    $this->incrementTotalHouseholdsInfected();
                }

                break;
            }
        }

        // Remove Zombies from pool after attack
        $this->zombiesYetToAttack -= $attackCount;

        return $this;
    }

    /**
     * Checks if any member of household was infected - 50% chance.
     * If an even number is generated a new Zombie will emerge.
     *
     * @return boolean
     */
    protected function isZombieSpawned()
    {
        return (bool)(mt_rand(1, 10) % 2);
    }

    /**
     * Returns value rounded up to the nearest attack value
     *
     * @param float $value
     *
     * @return float
     */
    protected function round($value)
    {
        return round($value / static::ATTACK_VALUE) * static::ATTACK_VALUE;
    }
}