<?php
namespace OutbreakSimulation;

/**
 * OutbreakSimulator class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
class OutbreakSimulator
{
    /**
     * Data Columns.
     *
     * @var array
     */
    public static $columns = [
        'Price'         => '1',
        'Property Type' => '4',
        'Build'         => '5',
    ];

    /**
     * Types of houses.
     *
     * @var array
     */
    public static $houseTypes = [
        'T' => '\OutbreakSimulation\Terraced',
        'S' => '\OutbreakSimulation\SemiDetached',
        'D' => '\OutbreakSimulation\Detached',
    ];

    /**
     * Program title
     *
     * @var string
     */
    protected $title = 'Outbreak Simulation';

    /**
     * Housing Price Data Source.
     *
     * @var string
     */
    protected $dataSource = 'http://prod.publicdata.landregistry.gov.uk.s3-website-eu-west-1.amazonaws.com/pp-2015.csv';

    /**
     * Population (Housing) Data.
     *
     * @var array
     */
    protected $housingData = [];

    /**
     * Total number of houses destroyed by horde.
     *
     * @var integer
     */
    protected $totalHousesDestroyed = 0;

    /**
     * Zombie Horde class.
     *
     * @var \OutbreakSimulation\ZombieHorde
     */
    protected $zombieHorde;

    /**
     * Zombie Horde class.
     *
     * @var \OutbreakSimulation\Report
     */
    protected $report;

    /**
     * Number of days of zombie attacks.
     *
     * @var int
     */
    protected $durationOfAttackToSimulate = 0;

    /**
     * How often report stats are logged.
     *
     * @var int
     */
    protected $reportIntervalInDays = 10;

    /**
     * Permits houses to defend themselves.
     *
     * @var boolean
     */
    protected $defenceAllowed = false;

    public function __construct()
    {
        $this->zombieHorde = new ZombieHorde;
        $this->report      = new Report;
    }

    /**
     * Run simlation.
     */
    public function run()
    {
        $this->report->welcomeMessage($this->title);
        $this->report->loadingMessage();
        $this->loadHousingData();
        $this->report->totalHousesMessage(count($this->housingData));
        $this->simulateZombieOutbreak();
        $this->report->goodbyeMessage();
    }

    /**
     * Set Initial population of zombie horde.
     *
     * @param int $dataSource Initial population of zombie horde
     */
    public function setDataSource($dataSource)
    {
        if ($dataSource) {
            $this->dataSource = $dataSource;
        }
    }

    /**
     * Set initial population of zombie horde.
     *
     * @param int $zombiePopulation Initial population of zombie horde
     */
    public function setZombieHordePopulation($zombiePopulation)
    {
        $this->zombieHorde->setZombieHordePopulation($zombiePopulation);
    }

    /**
     * Get initial population of zombie horde.
     *
     * @return integer
     */
    public function getZombieHordePopulation()
    {
        return $this->zombieHorde->getZombieHordePopulation();
    }

    /**
     * Set number of days of zombie attacks.
     *
     * @param int $days Number of days of zombie attacks
     */
    public function setDurationOfAttackToSimulate($days)
    {
        if ($days) {
            $this->durationOfAttackToSimulate = $days;
        }
    }

    /**
     * Set how often report stats are logged.
     *
     * @param int $days How often report stats are logged
     */
    public function setReportIntervalInDays($days)
    {
        if ($days) {
            $this->reportIntervalInDays = $days;
        }
    }

    /**
     * Permit houses to defend themselves.
     *
     * @param boolean $allowed
     */
    public function setDefenceAllowed($allowed = false)
    {
        $this->defenceAllowed = $allowed;
    }

    /**
     * Setup Simulation Environment.
     */
    protected function loadHousingData()
    {
        // Load Human Population Data
        if (($inputFileHandle = fopen($this->dataSource, 'r')) !== false) {
            while (($data = fgetcsv($inputFileHandle, 0, "\r")) !== false) {
                foreach ($data as $row) {
                    $row = explode(',', $row);

                    // Only load data for Terraced, SemiDetached and Detached properties.
                    $propertyType = $row[static::$columns['Property Type']];
                    if (! isset(static::$houseTypes[$propertyType])) {
                        continue;
                    }

                    $house = new static::$houseTypes[$propertyType](
                        $row[static::$columns['Price']],
                        $row[static::$columns['Build']]
                    );

                    $this->housingData[] = $house;
                }
            }

            fclose($inputFileHandle);
        }
    }

    /**
     * Simulate Zombie Outbreak.
     */
    protected function simulateZombieOutbreak()
    {
        $simulationDuration = $this->durationOfAttackToSimulate;
        $durationCount      = 0;

        // If no relevant houses where submitted there is nothing to run.
        $attack = (bool)count($this->housingData);

        $this->report->displayHeaders();

        $this->zombieHorde->setZombiesYetToAttack(
            $this->zombieHorde->getZombieHordePopulation()
        );

        while ($attack) {
            $durationCount++;

            // Randomly select a house to be attacked
            $index = array_rand($this->housingData, 1);
            $house = $this->housingData[$index];

            // Kill zombies before they damage house - if defences are active.
            if ($this->defenceAllowed && $house->activateDefences()) {
                $this->zombieHorde->decreaseZombieHorde(
                    $house->getNumberOfZombiesKilled()
                );
            }

            $this->zombieHorde->attack($house);

            // Check if house has been completely destroyed.
            if ($house->getPrice() < ZombieHorde::ATTACK_VALUE) {
                // Increment the number of destroyed houses
                $this->totalHousesDestroyed++;

                // Remove house from pool
                unset($this->housingData[$index]);
            } else {
                $this->housingData[$index] = $house;
            }

            $simulationDuration--;

            $remainingHouses  = count($this->housingData);
            $housesDestroyed  = $this->totalHousesDestroyed;
            $zombiePopulation = $this->zombieHorde->getZombieHordePopulation();
            $infections       = $this->zombieHorde->getTotalHouseholdsInfected();
            $damageDealt      = $this->zombieHorde->getTotalValueOfDamageDealt();

            if (! $simulationDuration || ! $remainingHouses) {
                if (! $remainingHouses) {
                    // If there are no more houses, then it is the end of days.
                    $durationCount = $remainingHouses;
                }

                $attack = false;
            }

            if (($durationCount % $this->reportIntervalInDays) == 0 || ! $attack) {
                $this->report->printReport(
                    $durationCount,
                    $remainingHouses,
                    $housesDestroyed,
                    $zombiePopulation,
                    $infections,
                    $damageDealt
                );

                // Reset number of households infected after report is displayed.
                $this->zombieHorde->resetTotalHouseholdsInfected();
            }
        }
    }
}