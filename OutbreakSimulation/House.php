<?php
namespace OutbreakSimulation;

/**
 * House class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
abstract class House
{
    /**
     * Represents a house that is a new build.
     *
     * @var string
     */
    const NEW_BUILD = 'Y';

    /**
     * Represents a house that is an old build.
     *
     * @var string
     */
    const OLD_BUILD = 'N';

    /**
     * Value to increase value of a new house by - 25%.
     *
     * @var float
     */
    const NEW_BUILD_VALUE_FACTOR = '0.25';

    /**
     * Value to simulate 1in100 chance of self defence.
     *
     * @var integer
     */
    const SELF_DEFENCE_ACTVATION_KEY = '13';

    /**
     * Multiples of this value determines how many zombies get killed.
     *
     * @var Integer
     */
    const SELF_DEFENCE_MULTIPLE = '50000';

    /**
     * Identifies a house as a new build.
     *
     * @var bool
     */
    protected $isNewBuild = false;

    /**
     * House price
     *
     * @var integer
     */
    protected $price;

    /**
     * Maximum fraction of Zombie Horde that can attack.
     *
     * @var integer
     */
    protected $zombieFractionMultiplier;

    /**
     * @param integer $price Price of House
     * @param string  $build Is house a new or old build
     *
     * @return void
     */
    public function __construct($price, $build)
    {
        $this->isNewBuild = ($build == static::NEW_BUILD);
        $this->price      = $price;

        // If the house is a new build,
        // upgrade the value of the house by 25%
        if ($this->isNewBuild) {
            $valueIncrease = $this->price * static::NEW_BUILD_VALUE_FACTOR;
            $this->price += $valueIncrease;
        }
    }

    /**
     * Returns value with which maximum fraction of zombie horde that
     * can attack is calculated.
     *
     * @return integer
     */
    public function getZombieFractionMultiplier()
    {
        return $this->zombieFractionMultiplier;
    }

    /**
     * Sets price of house
     *
     * @param integer $price
     *
     * @return \OutbreakSimulation\House
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Returns Ppice of house
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Updates price of house
     *
     * @param integer $value
     *
     * @return \OutbreakSimulation\House
     */
    public function decreasePrice($value)
    {
        $this->price -= $value;

        return $this;
    }

    /**
     * Verifies if house defences are active.
     *
     * @return boolean
     */
    public function activateDefences()
    {
        return (mt_rand(1, 100) == static::SELF_DEFENCE_ACTVATION_KEY);
    }

    /**
     * Number of zombies killed by defences.
     *
     * @return integer
     */
    public function getNumberOfZombiesKilled()
    {
        // If remainder is not equal to multiple of value, ignore it.
        return floor($this->price / static::SELF_DEFENCE_MULTIPLE);
    }
}