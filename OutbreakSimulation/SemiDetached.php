<?php
namespace OutbreakSimulation;

/**
 * SemiDetached House class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
class SemiDetached extends House
{
    /**
     * {@inheritdoc}
     */
    protected $zombieFractionMultiplier = 0.15;
}