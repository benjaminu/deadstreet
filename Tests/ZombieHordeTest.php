<?php
namespace Tests;

use OutbreakSimulation\Terraced;
use OutbreakSimulation\SemiDetached;
use OutbreakSimulation\Detached;
use OutbreakSimulation\House;
use OutbreakSimulation\ZombieHorde;

/**
 * ZombieHordeTest class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
class ZombieHordeTest extends BaseTestCase
{
    /**
     * House price
     *
     * @var integer
     */
    protected $price = 70000;

    /**
     * Old/New house build.
     *
     * @var string
     */
    protected $build = 'N';

    /**
     * ZombieHorde class
     *
     * @var \OutbreakSimulation\ZombieHorde
     */
    protected $zombieHorde;

    /**
     * Setup Test Environment.
     */
    public function setUp()
    {
        $this->zombieHorde = new ZombieHorde;
    }

    /**
     * Test setZombieHordePopulation method.
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function testSetZombieHordePopulation()
    {
        $population = 2000;
        $this->zombieHorde->setZombieHordePopulation($population);

        $this->assertAttributeEquals(
            $population,
            'zombieHordePopulation',
            $this->zombieHorde
        );

        return $this->zombieHorde;
    }

    /**
     * Test getZombieHordePopulation method.
     *
     * @return void
     */
    public function testGetZombieHordePopulation()
    {
        $this->assertAttributeEquals(
            $this->zombieHorde->getZombieHordePopulation(),
            'zombieHordePopulation',
            $this->zombieHorde
        );
    }

    /**
     * Test setZombiesYetToAttack method.
     *
     * @depends testSetZombieHordePopulation
     *
     * @param \OutbreakSimulation\ZombieHorde $zombieHorde
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function testSetZombiesYetToAttack(ZombieHorde $zombieHorde)
    {
        $atackers = 1000;
        $zombieHorde->setZombiesYetToAttack($atackers);

        $this->assertAttributeEquals(
            $atackers,
            'zombiesYetToAttack',
            $zombieHorde
        );

        return $zombieHorde;
    }

    /**
     * Test getZombiesYetToAttack method.
     *
     * @return void
     */
    public function testGetZombiesYetToAttack()
    {
        $this->assertAttributeEquals(
            $this->zombieHorde->getZombiesYetToAttack(),
            'zombiesYetToAttack',
            $this->zombieHorde
        );
    }

    /**
     * Test decreaseZombieHorde method.
     *
     * @depends testSetZombiesYetToAttack
     *
     * @param \OutbreakSimulation\ZombieHorde $zombieHorde
     *
     * @return void
     */
    public function testDecreaseZombieHorde(ZombieHorde $zombieHorde)
    {
        $reduction           = 200;
        $decreasedPopulation = $zombieHorde->getZombieHordePopulation() - $reduction;
        $decreasedAttackers  = $zombieHorde->getZombiesYetToAttack() - $reduction;

        $zombieHorde->decreaseZombieHorde($reduction);

        $this->assertAttributeEquals(
            $decreasedPopulation,
            'zombieHordePopulation',
            $zombieHorde
        );

        $this->assertAttributeEquals(
            $decreasedAttackers,
            'zombiesYetToAttack',
            $zombieHorde
        );
    }

    /**
     * Test getTotalHouseholdsInfected method.
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function testGetTotalHouseholdsInfected()
    {
        $this->assertAttributeEquals(
            $this->zombieHorde->getTotalHouseholdsInfected(),
            'totalHouseholdsInfected',
            $this->zombieHorde
        );

        return $this->zombieHorde;
    }

    /**
     * Test incrementTotalHouseholdsInfected method.
     *
     * @depends testGetTotalHouseholdsInfected
     *
     * @param \OutbreakSimulation\ZombieHorde $zombieHorde
     *
     * @return \OutbreakSimulation\ZombieHorde
     */
    public function testIncrementTotalHouseholdsInfected(ZombieHorde $zombieHorde)
    {
        $totalHouseholdsInfected = $zombieHorde->getTotalHouseholdsInfected();
        $zombieHorde->incrementTotalHouseholdsInfected();
        $this->assertEquals(
            ++$totalHouseholdsInfected,
            $zombieHorde->getTotalHouseholdsInfected()
        );

        return $zombieHorde;
    }

    /**
     * Test resetTotalHouseholdsInfected method.
     *
     * @depends testIncrementTotalHouseholdsInfected
     *
     * @param \OutbreakSimulation\ZombieHorde $zombieHorde
     *
     * @return void
     */
    public function testResetTotalHouseholdsInfected(ZombieHorde $zombieHorde)
    {
        $zombieHorde->resetTotalHouseholdsInfected();
        $this->assertEquals(0, $zombieHorde->getTotalHouseholdsInfected());

        return $this;
    }

    /**
     * Test an Attack on a Terraced House.
     *
     * @return void
     */
    public function testTerracedHouseAttack()
    {
        $terracedHouse = new Terraced($this->price, $this->build);
        $this->zombieHorde->attack($terracedHouse);

        $this->assertLessThan($this->price, $terracedHouse->getPrice());
    }

    /**
     * Test an Attack on a SemiDetached House.
     *
     * @return void
     */
    public function testSemiDetachedHouseAttack()
    {
        $semiDetachedHouse = new SemiDetached($this->price, $this->build);
        $this->zombieHorde->attack($semiDetachedHouse);

        $this->assertLessThan($this->price, $semiDetachedHouse->getPrice());
    }

    /**
     * Test an Attack on a Detached House.
     *
     * @return \OutbreakSimulation\Detached
     */
    public function testDetachedHouseAttack()
    {
        $detachedHouse = new Detached($this->price, $this->build);
        $this->zombieHorde->attack($detachedHouse);

        $this->assertLessThan($this->price, $detachedHouse->getPrice());

        return $this->zombieHorde;
    }

    /**
     * Test getTotalValueOfDamageDealt method.
     *
     * @depends testDetachedHouseAttack
     *
     * @param \OutbreakSimulation\ZombieHorde $zombieHorde
     *
     * @return void
     */
    public function testGetTotalValueOfDamageDealt(ZombieHorde $zombieHorde)
    {
        $this->assertAttributeEquals(
            $zombieHorde->getTotalValueOfDamageDealt(),
            'totalValueOfDamageDealt',
            $zombieHorde
        );
    }
}