<?php
namespace Tests;

use OutbreakSimulation\Terraced;
use OutbreakSimulation\SemiDetached;
use OutbreakSimulation\Detached;
use OutbreakSimulation\House;
use OutbreakSimulation\ZombieHorde;

/**
 * HouseTest class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
class HouseTest extends BaseTestCase
{
    /**
     * Zombie Horde.
     *
     * @var integer
     */
    protected $zombieHorde = 10000;

    /**
     * House price
     *
     * @var integer
     */
    protected $price = 70000;

    /**
     * House price
     *
     * @var string
     */
    protected $build = 'N';

    /**
     * Test getZombieFractionMultiplier method for a Terraced house.
     *
     * @return void
     */
    public function testGetTerracedZombieFractionMultiplier()
    {
        $multiplier    = 0.1;
        $terracedHouse = new Terraced($this->price, $this->build);

        $this->assertEquals(
            $terracedHouse->getZombieFractionMultiplier(),
            $multiplier
        );
    }

    /**
     * Test getZombieFractionMultiplier method for a SemiDetached house.
     *
     * @return void
     */
    public function testGetSemiDetachedZombieFractionMultiplier()
    {
        $multiplier        = 0.15;
        $semiDetachedHouse = new SemiDetached($this->price, $this->build);

        $this->assertEquals(
            $semiDetachedHouse->getZombieFractionMultiplier(),
            $multiplier
        );
    }

    /**
     * Test getZombieFractionMultiplier method for a Detached house.
     *
     * @return void
     */
    public function testGetDetachedZombieFractionMultiplier()
    {
        $multiplier    = 0.2;
        $detachedHouse = new Detached($this->price, $this->build);

        $this->assertEquals(
            $detachedHouse->getZombieFractionMultiplier(),
            $multiplier
        );
    }

    /**
     * Test setPrice method.
     *
     * @return \OutbreakSimulation\SemiDetached
     */
    public function testSetPrice()
    {
        $newPrice          = 300000;
        $semiDetachedHouse = new SemiDetached($this->price, $this->build);

        $semiDetachedHouse->setPrice($newPrice);

        $this->assertAttributeEquals($newPrice, 'price', $semiDetachedHouse);

        return $semiDetachedHouse;
    }

    /**
     * Test getPrice method.
     *
     * @param \OutbreakSimulation\SemiDetached $semiDetachedHouse
     *
     * @depends testSetPrice
     *
     * @return void
     */
    public function testGetPrice(SemiDetached $semiDetachedHouse)
    {
        $this->assertAttributeEquals(
            $semiDetachedHouse->getPrice(),
            'price',
            $semiDetachedHouse
        );
    }

    /**
     * Test decrease price method.
     *
     * @return void
     */
    public function testDecreasePrice()
    {
        $detachedHouse = new Detached($this->price, $this->build);
        $detachedHouse->decreasePrice(ZombieHorde::ATTACK_VALUE);

        $newPrice = $this->price - ZombieHorde::ATTACK_VALUE;

        $this->assertEquals($newPrice, $detachedHouse->getPrice());
    }

    /**
     * Test getNumberOfZombiesKilled method.
     *
     * @return void
     */
    public function testGetNumberOfZombiesKilled()
    {
        $terracedHouse = new Terraced($this->price, $this->build);
        $deadZombies   = floor($this->price / House::SELF_DEFENCE_MULTIPLE);

        $this->assertEquals(
            $deadZombies,
            $terracedHouse->getNumberOfZombiesKilled()
        );
    }

    /**
     * Test isNewBuild feature.
     *
     * @return void
     */
    public function testIsNewBuild()
    {
        $build = 'Y';

        $terracedHouse = new Terraced($this->price, $build);

        $increase      = House::NEW_BUILD_VALUE_FACTOR * $this->price;
        $newBuildPrice = $this->price + $increase;

        $this->assertEquals($newBuildPrice, $terracedHouse->getPrice());
    }
}