<?php
namespace Tests;

use OutbreakSimulation\OutbreakSimulator;

/**
 * OutbreakSimulatorTest class.
 *
 * @author    Benjamin Ugbene <benjamin.ugbene@googlemail.com>
 * @copyright 2016 Benjamin Ugbene
 * @license   MIT
 */
class OutbreakSimulatorTest extends BaseTestCase
{
    /**
     * OutbreakSimulator class
     *
     * @var \OutbreakSimulation\OutbreakSimulator
     */
    protected $os;

    /**
     * Setup Test Environment.
     */
    public function setUp()
    {
        $this->os = new OutbreakSimulator;
        $this->os->setDataSource(dirname(__DIR__) . '/price-data/pp-sample.csv');
    }

    /**
     * Test loadHousingData method.
     *
     * @return void
     */
    public function testLoadHousingData()
    {
        $this->invokeMethod($this->os, 'loadHousingData');

        $this->assertAttributeNotEmpty('housingData', $this->os);
    }

    /**
     * Test setDurationOfAttackToSimulate method.
     *
     * @return \OutbreakSimulation\OutbreakSimulator
     */
    public function testSetDurationOfAttackToSimulate()
    {
        $durationOfAttackToSimulate = 100;
        $this->os->setDurationOfAttackToSimulate($durationOfAttackToSimulate);

        $this->assertAttributeEquals(
            $durationOfAttackToSimulate,
            'durationOfAttackToSimulate',
            $this->os
        );

        return $this->os;
    }

    /**
     * Test setZombieHordePopulation method.
     *
     * @depends testSetDurationOfAttackToSimulate
     *
     * @param \OutbreakSimulation\OutbreakSimulator $os
     *
     * @return \OutbreakSimulation\OutbreakSimulator
     */
    public function testSetZombieHordePopulation(OutbreakSimulator $os)
    {
        $zombieHordePopulation = 10000;
        $os->setZombieHordePopulation($zombieHordePopulation);

        $this->assertEquals(
            $zombieHordePopulation,
            $os->getZombieHordePopulation()
        );

        return $os;
    }

    /**
     * Test setReportIntervalInDays method.
     *
     * @depends testSetZombieHordePopulation
     *
     * @param \OutbreakSimulation\OutbreakSimulator $os
     *
     * @return \OutbreakSimulation\OutbreakSimulator
     */
    public function testSetReportIntervalInDays(OutbreakSimulator $os)
    {
        $reportIntervalInDays = 30;
        $os->setReportIntervalInDays($reportIntervalInDays);

        $this->assertAttributeEquals(
            $reportIntervalInDays,
            'reportIntervalInDays',
            $os
        );

        return $os;
    }

    /**
     * Test setDefenceAllowed method.
     *
     * @depends testSetReportIntervalInDays
     *
     * @param \OutbreakSimulation\OutbreakSimulator $os
     *
     * @return \OutbreakSimulation\OutbreakSimulator
     */
    public function testSetDefenceAllowed(OutbreakSimulator $os)
    {
        $defenceAllowed = true;
        $os->setDefenceAllowed($defenceAllowed);

        $this->assertAttributeEquals(
            $defenceAllowed,
            'defenceAllowed',
            $os
        );

        return $os;
    }

    /**
     * Test run method.
     *
     * @depends testSetDefenceAllowed
     *
     * @param \OutbreakSimulation\OutbreakSimulator $os
     *
     * @return void
     */
    public function testRun(OutbreakSimulator $os)
    {
        $os->run();
        $this->expectOutputRegex('/END OF SIMULATION!!/');
    }
}